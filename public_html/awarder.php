<?PHP

include_once ( 'php/common.php' ) ;

$q = preg_replace ( '/\D/' , '' , get_request ( 'q' , '' ) ) ;

print get_common_header('','Awarder') ;

if ( isset ( $_REQUEST['generate'] ) ) {
	$aq = explode ( ',' , get_request('aq','') ) ;
	print "<form method='post' action='./quick_statements.php'>" ;
	print "<textarea name='list' style='width:100%' rows=20>" ;
	foreach ( $aq AS $award ) {
		if ( !isset ( $_REQUEST["use_$award"] ) ) continue ;
		$year = trim ( get_request ( "year_$award" , '' ) ) ;
		$l = "Q$q\tP166\tQ$award" ;
		if ( strlen($year) == 4 ) $l .= "\tP585\t+0000000$year-01-17T00:00:00Z/9" ;
		print "$l\n" ;
	}
	print "</textarea>" ;
	print "<input type='submit' name='doit' value='Run QuickStatements' class='btn btn-primary' />" ;
	print "</form>" ;
	

	print get_common_footer() ;
	exit ( 0 ) ;
}

print "<form method='get' class='form form-inline inline-form'>
Wikidata item: <input type='text' name='q' value='$q' /> <input type='submit' name='doit' value='Do it!' class='btn btn-primary' />
</form>" ;

if ( $q == '' ) {
	print get_common_footer() ;
	exit ( 0 ) ;
}

// Get awards
$url = $wdq_internal_url . '?q=' . urlencode ( "tree[618779][][31,279]" ) ;
$j = json_decode ( file_get_contents ( $url ) ) ;
$awards = implode ( ',' , $j->items ) ;
unset ( $j ) ;

// Get sites
$wp = array() ;
$db_wd = openDB ( 'wikidata' , 'wikidata' ) ;
$sql = "SELECT * FROM wb_items_per_site WHERE ips_item_id=$q" ;
if(!$result = $db_wd->query($sql)) die('There was an error running the query [' . $db_wd->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()){
	$wp[] = array ( $o->ips_site_id , $o->ips_site_page ) ;
}

// Get existing
$has_link = array() ;
$sql = "select distinct pl_title FROM page,pagelinks WHERE page_title='Q$q' AND page_namespace=0 and pl_from=page_id AND pl_namespace=0" ;
if(!$result = $db_wd->query($sql)) die('There was an error running the query [' . $db_wd->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()){
	$q2 = preg_replace ( '/\D/' , '' , $o->pl_title ) ;
	$has_link[$q2] = $q2 ;
}


//$sitelinks = array() ;
$award_counter = array() ;
$aq = array() ;
foreach ( $wp AS $wiki ) {
	$site = $wiki[0] ;
	$page = $wiki[1] ;
//	$sitelinks[] = "<a href='//' target='_blank'>$site</a>" ;
//	print "$site: $page<br/>" ;
	
	// Get linked pages
	$db = openDBwiki ( $site ) ;
	$p2 = $db->real_escape_string ( str_replace ( ' ' , '_' , $page ) ) ;
	$sql = "select DISTINCT pl_title FROM page,pagelinks WHERE page_namespace=0 and page_title='$p2' AND pl_from=page_id AND pl_namespace=0" ;
	$targets = array() ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
	while($o = $result->fetch_object()){
		$title = str_replace ( '_' , ' ' , $o->pl_title ) ;
		$targets[$title] = $db_wd->real_escape_string ( $title ) ;
	}
	if ( count ( $targets ) == 0 ) continue ;
	
	$sql = "SELECT DISTINCT ips_item_id FROM wb_items_per_site WHERE ips_site_id='$site' AND ips_site_page IN ('" . implode("','",$targets) . "') AND ips_item_id IN ($awards)" ;
	if(!$result = $db_wd->query($sql)) die('There was an error running the query [' . $db_wd->error . ']'."\n$sql\n\n");
	while($o = $result->fetch_object()){
		$q2 = $o->ips_item_id ;
		$aq[$q2] = $q2 ;
		$award_counter[$q2]++ ;
//		print "=> " . $o->ips_item_id . "<br/>" ;
	}
}

if ( count ( $aq ) == 0 ) {
	print "Could not find any awards for Q$q" ;
	print get_common_footer() ;
	exit ( 0 ) ;
}

$award_titles = array() ;
$langlist = "'en','de','es','fr','it','ru'" ;
$sql = "SELECT DISTINCT term_entity_id,term_text FROM wb_terms WHERE term_language IN ($langlist) AND term_type='label' AND term_entity_id IN (" . join(',',$aq) . ") ORDER BY field(term_language,$langlist)" ;
if(!$result = $db_wd->query($sql)) die('There was an error running the query [' . $db_wd->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()){
	if ( isset($award_titles[$o->term_entity_id]) ) continue ;
	$award_titles[$o->term_entity_id] = $o->term_text ;
}

print "<h2>Awards linked from Wikipedia articles for <a href='//www.wikidata.org/wiki/Q$q' target='_blank'>Q$q</a></h2>" ;
print "<form method='post' action='?'>" ;
print "<table class='table table-condensed table-striped'><tbody>" ;
foreach ( $aq AS $award ) {
	$at = $award_titles[$award] ;
	if ( !isset($at) ) $at = "Q$award" ;
	print "<tr>" ;
	print "<td><input type='checkbox' name='use_$award'  /></td>" ; // checked
	print "<td>" . $award_counter[$award] . "&times;</td>" ;
	print "<td><a href='//www.wikidata.org/wiki/Q$award' target='_blank'>$at</a></td>" ;
	print "<td><input type='text' placeholder='Year' name='year_$award' /></td>" ;
	if ( isset($has_link[$award]) ) print "<td>Already in item</td>" ;
	else print "<td></td>" ;
	print "</tr>" ;
}
print "</tbody></table>" ;
print "<input type='hidden' name='q' value='$q' />" ;
print "<input type='hidden' name='aq' value='" . implode(',',$aq) . "' />" ;
print "<input type='submit' class='btn btn-primary' value='Generate statements' name='generate' />" ;
print "</form>" ;

print get_common_footer() ;

?>