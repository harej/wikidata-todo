<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','500M');

include_once ( "php/common.php" ) ;

$prop = preg_replace ( '/\D/' , '' , get_request ( 'prop' , '' ) ) ;
$value = trim ( get_request ( 'value' , '' ) ) ;

$j = '' ;

if ( $prop != '' and $value != '' ) {
	$j = json_decode ( file_get_contents ( "$wdq_internal_url?q=" . urlencode('string['.$prop.':"'.$value.'"]') ) ) ;
	if ( count ( $j->items ) == 1 ) {
		header('Content-type: text/html; charset=UTF-8'); // UTF8 test
		header("Cache-Control: no-cache, must-revalidate");
		print "<html>\n<meta HTTP-EQUIV='REFRESH' content='0; url=https://www.wikidata.org/wiki/Q" . $j->items[0] . "'></head></html>" ;
		exit ( 0 ) ;
	}
}

print get_common_header ( '' , 'Wikidata Resolver' ) ;

print "<div class='lead'>Redirect to a Wikidata item, based on a string value for a property.</div>" ;

if ( $j != '' and count ( $j->items ) > 1 ) {
	print "<div>Multiple Wikidata items with P$prop:\"$value\" :<ol>" ;
	foreach ( $j->items AS $i ) {
		print "<li><a href='//www.wikidata.org/wiki/Q$i' target='_blank'>Q$i</a></li>" ;
	}
	print "</ol>" ;
}

print "<form method='get' class='form inline-form form-inline' action='?'>
<input type='text' name='prop' placeholder='Property (e.g. P227)' value='$prop' size='30' />
<input type='text' name='value' placeholder='Value (e.g. 119186187)' value='$value' size='80' />
<input type='submit' value='Find Wikidata item' class='btn btn-primary' />
</form>" ;

print get_common_footer() ;

?>