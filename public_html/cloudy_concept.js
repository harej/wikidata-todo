function escattr ( s ) {
	return s.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(/"/g,'&quot;').replace(/'/g,'&#x27;').replace(/\//g,'&#x2F;') ;
}

var cloudy_concept = {

	widar_url : '/widar/index.php?callback=?' ,

	init : function () {
		var self = this ;
		this.wd = new WikiData ;
		this.params = this.getUrlVars() ;
		this.q = this.params.q ;
		this.lang = (this.params.lang||'en') ;
		this.wd.main_languages.unshift ( this.lang ) ;
		this.wd.getItemBatch ( to_load , function () {
			self.addHoverboxes() ;
		} ) ;
	} ,

	getUrlVars : function () {
		var vars = {} ;
		var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1) ;
		var hash = window.location.href.slice(window.location.href.indexOf('#') + 1) ;
		if ( hash == window.location.href ) hash = '' ;
		if ( hash.length > 0 ) hashes = hash ;
		else hashes = hashes.replace ( /#$/ , '' ) ;
		hashes = hashes.split('&');
		$.each ( hashes , function ( i , j ) {
			var hash = j.split('=');
			hash[1] += '' ;
			vars[hash[0]] = decodeURI(hash[1]).replace(/_/g,' ');
		} ) ;
		return vars;
	} ,

	getMainLang : function () {
		var self = this ;
		return self.wd.main_languages[0] ;
	} ,

	hasNoLabelInMainLanguage : function ( item ) {
		var self = this ;
		var ml = self.getMainLang() ;
		if ( item.raw === undefined ) return true ;
		if ( item.raw.labels === undefined ) return true ;
		if ( item.raw.labels[ml] === undefined ) return true ;
		return false ;
	} ,

	addHoverboxes : function  ( selector ) {
		if ( selector === undefined ) selector = '' ;
		var self = this ;
		var pl = self.lang ;
		wd_auto_desc.lang = pl ;
		var icons = {
			wiki:'https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Wikipedia-logo-v2.svg/18px-Wikipedia-logo-v2.svg.png' ,
			wikivoyage:'https://upload.wikimedia.org/wikipedia/commons/thumb/8/8a/Wikivoyage-logo.svg/18px-Wikivoyage-logo.svg.png' ,
			wikisource:'https://upload.wikimedia.org/wikipedia/commons/thumb/4/4c/Wikisource-logo.svg/18px-Wikisource-logo.svg.png'
		} ;

		$(selector+' a.q_internal').cluetip ( { // http://plugins.learningjquery.com/cluetip/#options
			splitTitle:'|',
			multiple : false,
			sticky : true ,
			mouseOutClose : 'both' ,
			cluetipClass : 'myclue' ,
			leftOffset : 0 ,
//				delayedClose : 500 ,
			onShow : function(ct, ci) { // Outer/inner jQuery node
				$('div.maps div').css({'z-index':0}) ; // 
				var a = $(this) ;
				var qnum = a.attr('q').replace(/\D/g,'') ;
				var q = 'Q'+qnum ;
				var i = self.wd.items[q] ;
				var title = i.getLabel() ;
				var dl = i.getLabelDefaultLanguage() ;
				var rank = a.hasClass('rank_deprecated') ? 'deprecated' : ( a.hasClass('rank_preferred') ? 'preferred' : 'normal' ) ;

				var h = "" ;
				h += "<div><span style='margin-right:10px;font-size:12pt'><a class='wikidata' target='_blank' href='//www.wikidata.org/wiki/Q"+qnum+"'>Q"+qnum+"</a></span>" ;
				
				var sl = i.getWikiLinks() ;
				$.each ( [ 'wiki' , 'wikivoyage' , 'wikisource' ] , function ( dummy , site ) {
					var s2 = site=='wiki'?'wikipedia':site ;
					if ( sl[pl+site] != undefined ) h += "<span style='margin-left:5px'><a title='Open in "+pl+"' target='_blank' href='//"+pl+"."+s2+".org/wiki/"+escape(sl[pl+site].title)+"'><img border=0 src='"+icons[site]+"'/></a></span>" ;
				} ) ;

				var commons = i.getClaimObjectsForProperty ( 373 ) ;
				if ( commons.length > 0 ) {
					h += "<span style='margin-left:5px'><a title='Commons category' target='_blank' href='//commons.wikimedia.org/wiki/Category:"+escape(commons[0].s)+"'><img src='https://upload.wikimedia.org/wikipedia/commons/thumb/4/4a/Commons-logo.svg/18px-Commons-logo.svg.png' border=0 /></a></span>" ;
				}
				h += "</div>" ;
				
				
				if ( self.hasNoLabelInMainLanguage(i) ) {
					h += "<div class='internal missing_label'><i>" ;
					h += "No label in "+pl;//self.t('no_label_in').replace(/\$1/g,self.all_languages[pl]||pl) ;
					h += "</i>" ;

					h += "<br/><a href='#' onclick='cloudy_concept.addLabelOauth(\""+q+"\",\""+pl+"\",\""+escattr(i.getLabel())+"\");return false'><b>" ;
					h += 'Add a label' + "</b></a> (" + 'via $1'.replace(/\$1/,"<a target='_blank' href='/widar'>WiDaR</a>") + ")" ;

					h += "</div>" ;
				}

				h += "<div>" + i.getDesc() + "</div>" ;
			
				var adid = 'cluetip_autodesc_'+q ;
				if ( self.use_autodesc ) {
					h += "<div id='"+adid+"'>...</div>" ;
				}
				
				// Rank
//				if ( rank != 'normal' ) h += "<div>" + self.t('rank_label') + " : " + self.t('rank_'+rank) + "</div>" ;
			
			
				ct.css({'background-color':'white'}) ; // TODO use cluetipClass for real CSS
				var title_element = $(ct.find('h3')) ;
				title_element.html ( title ) ;
				ci.attr({q:q}) ;
				ci.html ( h ) ;
				if ( self.isRTL ) ci.css ( { 'direction':'RTL' } ) ;

				var images = i.getMultimediaFilesForProperty(18);
				if ( images.length > 0 ) {
					var img = images[0] ;
					$.getJSON ( '//commons.wikimedia.org/w/api.php?callback=?' , {
						action:'query',
						titles:'File:'+img,
						prop:'imageinfo',
						format:'json',
						iiprop:'url',
						iiurlwidth:120,
						iiurlheight:300
					} , function ( d ) {
						if ( d.query === undefined || d.query.pages === undefined ) return ;
						$.each ( d.query.pages , function ( dummy , v ) {
							if ( v.imageinfo === undefined ) return ;
							var ii = v.imageinfo[0] ;
							var h = "<div style='float:right'><img src='"+ii.thumburl+"' /></div>" ;
							if ( ci.attr('q') != q ) return ;
							$('#cluetip').css({width:'400px'});
							ci.prepend ( h ) ;
						} ) ;
					} ) ;
				}

				if ( self.use_autodesc ) {
					wd_auto_desc.loadItem ( q , { target:$('#'+adid) , reasonator_lang:(lang||'en') , links:'reasonator_local' } ) ;
				}
			}
		} ) ;
		
//		console.log ( "hoverboxes added" ) ;
	} ,
		
	
	addLabelOauth : function ( q , lang , basis , callback ) {
		var a = $('a.q_internal[q="'+q.replace(/\D/g,'')+'"]') ;
		var self = cloudy_concept ;
		var label = prompt ( "New label in " + lang , (basis||"") ) ;
		
		if(label == "" || label == null) return ;
		
		var oauth_wait_dialog = "<div id='oauth_wait_dialog' style='width: 200px; position: absolute; top: 200px; background-color: #99C7FF; color: white; text-align: center; left: 50%; margin-left: -100px; padding: 5px;'>Editing via WiDaR...</div>" ;
		$('#oauth_wait_dialog').remove() ;
		$('body').append ( oauth_wait_dialog ) ;

		$.getJSON ( self.widar_url , {
			action:'set_label',
			q:q,
			lang:lang,
			label:label,
			botmode:1
		} , function ( d ) {
			$('#oauth_wait_dialog').remove() ;
			if ( d.error == 'OK' ) {
				a.removeClass ( 'missing_label' ) ; //.css({border:'none'}).text ( label ) ;
				cloudy_concept.wd.items[q].raw.labels[lang] = { language:lang , value:label } ;
				if ( callback !== undefined ) callback ( label ) ;
			} else alert ( d.error ) ;
		} ) .fail(function( jqxhr, textStatus, error ) {
			alert ( error ) ;
		} ) ;
	} ,

} ;

$(document).ready ( function() {
	cloudy_concept.init() ;
} ) ;
