<?PHP

//error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
//ini_set('display_errors', 'On');

require_once ( 'php/common.php' ) ;
include_once ( 'php/wikiquery.php' ) ;

$tsf = "D, d M Y H:i:s " ;
$tstz = "GMT" ;
$ts = date ( $tsf ) . $tstz  ;

$wdq = trim ( get_request ( 'wdq' , '' ) ) ;
$max = 1 * get_request ( 'max' , '50' ) ;
$thumbsize = 1 * get_request ( 'thumbsize' , '300' ) ;

if ( !isset($_REQUEST['doit']) or $wdq == '' ) {
	print get_common_header ( '' , 'WDQ image feed' ) ;
	print "<div class='lead'>Generate an image RSS feed (for your screensaver?) from a <a href='//wdq.wmflabs.org'>WDQ query</a>!</div>
	<form method='get' class='form form-inline inline-form' action='?'>
	<input type='text' name='wdq' size='60' placeholder='Type WDQ query here!' value='$wdq'/>
	<input type='submit' name='doit' value='Generate feed' class='btn btn-primary' />
	<br/><input size='10' type='number' name='thumbsize' value='$thumbsize' />px thumb size
	</form>" ;
	print "<div>Of all returned items with images, a random subset of 50 will be used.</div>" ;
	print "<div>Example: <a href='?wdq=claim[31%3A%28TREE[12280][][279]%29]+AND+claim[177%3A1653]&doit=Generate+feed'>Bridges across the Danube</a></div>" ;
	exit ( 0 ) ;
}

function xmlify ( $text ) {
	$doc = new DOMDocument();
	$fragment = $doc->createDocumentFragment();
	$fragment->appendChild($doc->createTextNode($text));
	return $doc->saveXML($fragment);
}

function get_thumb ( $image ) {
	global $thumbsize ;
	$wq = new WikiQuery ( 'commons' , 'wikimedia' ) ;
	$ret = $wq->get_image_data ( "File:$image" , round ( $thumbsize ) ) ;
	$ret = $ret['imageinfo'][0]['thumburl'] ;
	return $ret ;
}

if ( 1 ) header('Content-type: application/rss+xml; charset=utf-8');
else header('Content-Type: text/plain'); 


$params = array() ;

$params[] = "wdq=" . urlencode ( $wdq ) ;
$params[] = "doit=1" ;

$j = json_decode ( file_get_contents ( "$wdq_internal_url?q=".urlencode("($wdq) AND CLAIM[18]")."&props=18") ) ;

print '<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">
<channel>

<atom:link href="//tools.wmflabs.org/wikidata-todo/wdq_image_feed.php?' . xmlify(implode('&',$params)) . '" rel="self" type="application/rss+xml" /><title>' . xmlify($wdq) . ' - www.wikidata.org</title>
<link>http://tools.wmflabs.org/catfood/catfood.php</link><description>Query ' . xmlify($wdq) . ' for www.wikidata.org</description><language>en-us</language>
<copyright>Feed: CC-0; Images: see description page</copyright><pubDate>Thu, 19 Mar 2015 00:46:21 GMT</pubDate><lastBuildDate>Thu, 19 Mar 2015 00:46:21 GMT</lastBuildDate><generator>CatFood</generator>
<docs>http://tools.wmflabs.org/wikidata-todo/wdq_image_feed.php</docs>


<image>
<url>//upload.wikimedia.org/wikipedia/commons/thumb/6/66/Wikidata-logo-en.svg/200px-Wikidata-logo-en.svg.png</url>
<title>' . xmlify(implode('&',$params)) . '</title>
<link>http://tools.wmflabs.org/wikidata-todo/wdq_image_feed.php</link>
</image>' ;

/*
<item>
 <title>Lightmatter kimodo.jpg</title>
 <pubDate>Wed, 25 Feb 2015 04:35:31 GMT</pubDate>
 <link>http://commons.wikimedia.org/wiki/File:Lightmatter_kimodo.jpg</link>
 <guid isPermaLink="false">http://commons.wikimedia.org/wiki/File:Lightmatter_kimodo.jpg - Wed, 25 Feb 2015 04:35:31 GMT</guid>
 <description>
&lt;a href="http://commons.wikimedia.org/wiki/File:Lightmatter_kimodo.jpg"&gt;&lt;img border="0" src="http://upload.wikimedia.org/wikipedia/commons/thumb/9/9d/Lightmatter_kimodo.jpg/300px-Lightmatter_kimodo.jpg" /&gt;&lt;/a&gt;&lt;br/&gt;
Uploaded by user "&lt;a href="http://commons.wikimedia.org/wiki/User:Quadell"&gt;Quadell&lt;/a&gt;" on Sat, 13 Nov 2004 20:07:00 GMT&lt;br/&gt;
Added to category on Wed, 25 Feb 2015 04:35:31 GMT&lt;br/&gt;
Original image: 720&amp;times;480 pixel; 105.132 bytes.&lt;br/&gt;
Licensing : &lt;a href="http://en.wikipedia.org/wiki/CC-BY"&gt;CC-BY&lt;/a&gt;
 </description>
</item>
' ;
*/


$eighteen = '18' ;
$a = (array) $j->props->$eighteen ;
shuffle ( $a ) ;
$images = array_slice ( $a , 0 , $max ) ;

$db = openDB ( 'commons' , "wikimedia" ) ;
$sql = array() ;
foreach ( $images AS $v ) {
	$v[2] = str_replace(' ','_',$v[2]) ;
	$sql[] = $db->real_escape_string ( $v[2] ) ;
}
$sql = "SELECT * FROM image WHERE img_name IN ('" . implode("','",$sql) . "')" ;
if(!$result = $db->query($sql)) { die($db->error); } // TODO error handling
$meta = array() ;
while($o = $result->fetch_object()){
	$meta[$o->img_name] = $o ;
}



foreach ( $images AS $v ) {
	$v[2] = str_replace(' ','_',$v[2]) ;
	$q = $v[0] ;
	$i = $v[2] ;
	$iurl = xmlify('http://commons.wikimedia.org/wiki/File:'.myurlencode($i)) ;
	$turl = xmlify ( get_thumb ( $i ) ) ;
	$user = $meta[$i]->img_user_text ;
print '<item>
<title>' . xmlify(str_replace('_',' ',$i)) . '</title>
<pubDate>' . $ts . '</pubDate>
<link>' . $iurl . '</link>
<guid isPermaLink="false">' . $iurl . ' - ' . $ts . '</guid>
<description>
&lt;a href="' . $iurl . '"&gt;&lt;img border="0" src="' . $turl . '" /&gt;&lt;/a&gt;&lt;br/&gt;' ;

	if ( $user != '' ) {
		print 'Uploaded by &lt;a href="//commons.wikimedia.org/wiki/User:'.myurlencode($user).'"&gt;'.xmlify($user).'&lt;/a&gt;&lt;br/&gt;' ;
	}
	print 'Used by Wikidata item &lt;a href="//www.wikidata.org/wiki/Q'.$q.'"&gt;Q'.$q.'&lt;/a&gt;&lt;br/&gt;' ;
	print '</description></item>' ;
}

// https://tools.wmflabs.org/catfood/catfood.php?language=commons&project=wikimedia&category=Varanus+komodoensis&depth=0&namespace=6&user=&size=300&last=10&doit=Do+it


print '</channel></rss>' ;

?>