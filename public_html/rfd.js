jQuery(document).ready ( function() {
	if ( mw.config.get('wgNamespaceNumber') != 4 ) return ;
	if ( mw.config.get('wgPageName') != 'Wikidata:Requests_for_deletions' ) return ;
	if ( mw.config.get('wgAction') != 'view' ) return ;
	
	var to ;
	
	function startTheClock () {
		var id = $('div.rfd_helper').attr('id') ;
		to = setTimeout ( function () {
			$('#'+id).remove() ;
		} , 500 ) ;
	}

	function stopTheClock () {
		if ( typeof to != 'undefined' ) { clearTimeout(to) ; to = undefined ; }
	}
 
	$('h2 a').mouseenter ( function () {
		stopTheClock() ;
		var o = $($(this).parents('h2').get(0)) ;
		var q = $(this).text() ;
		var id = 'rfd_helper_'+q ;
		if ( $('#'+id).length > 0 ) return ; // This already exists
		$('div.rfd_helper').remove() ;
		var h = '' ;
		h += "<div class='rfd_helper' id='"+id+"'>" ;
		h += "<div class='loading'><i>Loading...</i></div>" ;
		h += "</div>" ;
		o.after(h) ;
		
		$('#'+id).mouseenter ( stopTheClock ) ;
		$('#'+id).mouseleave ( startTheClock ) ;
		
		var loading = 3 ;
		
		$.get ( '/w/api.php' , {
			action:'query',
			list:'backlinks',
			bltitle:q,
			bllimit:500,
			blnamespace:0,
			format:'json'
		} , function ( d ) {
			var h2 = "<div><b>Links to this item:</b> " ;
			var items = [] ;
			$.each ( ((d.query||{}).backlinks||{}) , function ( k , v ) { items.push ( "<a href='/wiki/"+v.title+"'>"+v.title+"</a>" ) } ) ;
			if ( items.length == 0 ) h2 += "<i>none</i>" ;
			else h2 += items.join(', ') ;
			h2 += "</div>" ;
			$('#'+id).prepend ( h2 ) ;
			loading-- ;
			if ( loading == 0 ) $('#'+id+' div.loading').remove() ;
		} , 'json' ) ;

		$.get ( '/w/api.php' , {
			action:'query',
			list:'logevents',
			letitle:q,
			lelimit:500,
			leprop:'type|user|timestamp',
			format:'json'
		} , function ( d ) {
			var h2 = "<div><b>Log events:</b> " ;
			var items = [] ;
			$.each ( ((d.query||{}).logevents||{}) , function ( k , v ) {
				if ( v.type == 'patrol' ) return ; // Skip patrols
				var h3 = "<div>" ;
				h3 += "<i>" + v.type + "/" + v.action + "</i>" ;
				h3 += " by <a href='/wiki/User:" + encodeURIComponent(v.user) + "'>" + v.user + "</a>" ;
				h3 += " on " + v.timestamp ;
				h3 += "</div>" ;
				items.push ( h3 )
			} ) ;
			if ( items.length == 0 ) h2 += "<i>none</i> <small>(not counting patrols)</small>" ;
			else h2 += items.join('') ;
			h2 += "</div>" ;
			$('#'+id).prepend ( h2 ) ;
			loading-- ;
			if ( loading == 0 ) $('#'+id+' div.loading').remove() ;
		} , 'json' ) ;

		$.get ( '/w/api.php' , {
			action:'query',
			prop:'revisions',
			titles:q,
			rvlimit:5,
			rvprop:'timestamp|user|comment',
			format:'json'
		} , function ( d ) {
			var h2 = "<div><b>Recent history:</b> " ;
			var items = [] ;
			$.each ( ((d.query||{}).pages||{}) , function ( k0 , v0 ) {
				$.each ( (v0.revisions||[]) , function ( k , v ) {
					var h3 = "<div>" ;
					h3 += v.timestamp ;
					h3 += " by <a href='/wiki/User:" + encodeURIComponent(v.user) + "'>" + v.user + "</a>" ;
					h3 += " : " + v.comment ;
					h3 += "</div>" ;
					items.push ( h3 )
				} ) ;
			} ) ;
			if ( items.length == 0 ) h2 += "<i>none</i>" ;
			else h2 += items.join('') ;
			h2 += "</div>" ;
			$('#'+id).prepend ( h2 ) ;
			loading-- ;
			if ( loading == 0 ) $('#'+id+' div.loading').remove() ;
		} , 'json' ) ;
		
		
	} ) ;

	$('h2 a').mouseleave ( startTheClock ) ;
});