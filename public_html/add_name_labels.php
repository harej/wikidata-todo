<?PHP

include ( "php/common.php" ) ;

$db = openDB ( 'wikidata' , 'wikidata' ) ;
$from_lang = $db->real_escape_string ( get_request ( 'from' , 'es,fr' ) ) ;
$missing_lang = $db->real_escape_string ( get_request ( 'to' , 'en' ) ) ;
$wdq = get_request ( 'wdq' , '' ) ;

print get_common_header ( '' , 'Names as labels' ) ;

print "
<form method='get'>
From language with labels <input type='text' name='from' value='$from_lang' />
to language without labels <input type='text' name='to' value='$missing_lang' />
<input type='submit' value='Do it!' name='doit' class='btn btn-primary' />
<br/>
<i>or</i> <input type='text' name='wdq' value='$wdq' placeholder='WDQ' />
</form>
" ;

if ( !isset($_REQUEST['doit']) ) exit ( 0 ) ;

$wiki = $from_lang . 'wiki' ;
if ( $wdq == '' ) $wdq = "claim[31:5] and link[$wiki]" ;
$url = $wdq_internal_url . "?q=" . urlencode ( $wdq ) ;
$json = json_decode ( file_get_contents ( $url ) ) ;

//$sql = "SELECT term_entity_id FROM wb_terms WHERE term_language='$missing_lang' AND term_type='label' AND term_entity_id IN (" . implode(',',$json->items) . ")" ;
$sql = "SELECT * FROM wb_terms t1 WHERE " ;

if ( $from_lang != '' ) $sql .= " t1.term_language IN (" . "'" . implode ( "','" , explode ( ',' , $from_lang ) ) . "' ) AND " ;

$sql .= " t1.term_type='label' AND t1.term_entity_id IN (" . implode(',',$json->items) . ") AND NOT EXISTS (SELECT * FROM wb_terms t2 WHERE t1.term_entity_id=t2.term_entity_id AND t2.term_language='$missing_lang' AND t2.term_type='label')" ;
$sql .= " ORDER BY term_entity_id" ;

$last = '' ;
print "<textarea style='width:100%' rows='20'>" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	if ( $last == $o->term_entity_id ) continue ; // Avoid duplicate items
	$last = $o->term_entity_id ;
	$label = $o->term_text ;
	$label = ucfirst ( trim ( preg_replace ( '/\s*\(.*$/' , '' , $label ) ) ) ;
	print "Q" . $o->term_entity_id . "\tL$missing_lang\t\"$label\"\n" ;
}
print "</textarea>" ;

print get_common_footer() ;

?>