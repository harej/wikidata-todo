#!/usr/bin/perl

use strict ;
use warnings ;

my $overwrite ;#= 1 ;

my %hadthat ;
my $d1 = '/public/dumps/public/wikidatawiki' ;
my $d2 = '/shared/dumps' ;

=cut
my $first = ( -d $d2 ) ? 1 : 0 ;
foreach my $date ( `ls $d1` ) {
	chomp $date ;
	my $file = "$d1/$date/wikidatawiki-$date-pages-articles.xml.bz2" ;
	if ( $first ) {
		my $fn = "$d2/" . $date . "_articles_pages.bz2" ;
		unless ( -e $fn ) {
			print "Preserving file for $date\n" ;
			`cp $file $fn` ;
		}
		$first = 0 ;
	}
	process ( $date , $file ) ;
}
=cut

if ( -d $d2 ) {
	foreach my $f ( `ls $d2` ) {
		chomp $f ;
		next unless $f =~ m/(20\d{6}).json.gz/ ;
		process ( $1 , "$d2/$f" ) ;
	}
}

0 ;

sub process {
	my ( $date , $fn ) = @_ ;
	return if defined $hadthat{$date} ;
	$hadthat{$date} = 1 ;
	return unless -e $fn ;
	my $outfile = "/data/project/wikidata-todo/stats/data/$date.json" ;
	return if -e $outfile and not defined $overwrite ;
	my $cmd = "/data/project/wikidata-todo/stats/job.sh $date $fn" ;
#	print "$cmd\n" ;
	`$cmd` ;
}
