#!/usr/bin/php
<?PHP

require_once ( '/data/project/wikidata-todo/public_html/php/common.php' ) ;

$langs = array ( 'de' ) ; //, 'es' , 'it' , 'ru' , 'en' , 'nl' ) ;

$dir = '../public_html/wp_no_image' ;
$head = "<!doctype html>\n<html><head><meta charset='utf-8'></head><body><ol>" ;
$foot = "</ol></body></html>" ;

foreach ( $langs AS $lang ) {
	$project = 'wikipedia' ;
	$wiki = $lang.'wiki' ;
	$db = openDBwiki ( $wiki ) ;
	$fh = fopen ( "$dir/$wiki.html" , 'w' ) ;
	fwrite ( $fh , $head ) ;
	$sql = 'select distinct page_title from page WHERE NOT EXISTS ( SELECT * FROM imagelinks where il_from=page_id ) and page_namespace=0 and page_is_redirect=0 and page_title in (select distinct(replace(ips_site_page," ","_")) from wikidatawiki_p.wb_entity_per_page,wikidatawiki_p.wb_items_per_site,wikidatawiki_p.pagelinks where ips_site_id="'.$wiki.'" and ips_item_id=epp_entity_id and epp_entity_type="item" and pl_from=epp_page_id and pl_namespace=120 and pl_title="P18")' ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		fwrite ( $fh , "<li><a target='_blank' href='//$lang.$project.org/wiki/".urlencode($o->page_title)."'>".str_replace('_',' ',$o->page_title)."</a></li>" ) ;
	}
	fwrite ( $fh , $foot ) ;
	fclose ( $fh ) ;
}

$fh = fopen ( "$dir/index.html" , 'w' ) ;
fwrite ( $fh , $head ) ;
foreach ( $langs AS $lang ) {
	$wiki = $lang.'wiki' ;
	fwrite ( $fh , "<li><a href='$wiki.html'>$wiki</a></li>" ) ;
}
fwrite ( $fh , $foot ) ;
fclose ( $fh ) ;


?>