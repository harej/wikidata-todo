#!/usr/bin/php
<?PHP

include_once ( '../public_html/php/common.php' ) ;

$wikis = array ( 'enwiki' , 'dewiki' , 'itwiki' , 'eswiki' , 'nlwiki' ) ;

$fn = fopen ( '/data/project/wikidata-todo/scripts/wiki_coords.tab' , 'w' ) ;
foreach ( $wikis AS $wiki ) {
	$db = openDBwiki ( $wiki ) ;
	$sql = 'select page_title,el_to,ips_item_id from page,externallinks,wikidatawiki_p.wb_items_per_site,wikidatawiki_p.wb_entity_per_page where el_from=page_id and page_namespace=0 and ( el_index like "http://org.wmflabs.tools./geohack/geohack.php?%" or el_index like "https://org.wmflabs.tools./geohack/geohack.php?%" ) AND replace(page_title,"_"," ")=ips_site_page AND ips_item_id=epp_entity_id and epp_entity_type="item" and ips_site_id="'.$wiki.'" AND epp_page_id NOT IN ( select DISTINCT pl_from from wikidatawiki_p.pagelinks where ( pl_namespace=120 and pl_title="P625" ) OR (pl_namespace=0 and pl_title IN ("Q4167410","Q5")) )' ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()){
		$row = array ( $wiki , $o->page_title , 'Q'.$o->ips_item_id , $o->el_to ) ;
		fwrite ( $fh , implode ( "\t" , $row ) . "\n" ) ;
	}
}
fclose ( $fh ) ;

?>
